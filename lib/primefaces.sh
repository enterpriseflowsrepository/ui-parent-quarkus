#!/bin/bash

mvn install:install-file \
  -DgroupId=org.primefaces \
  -DartifactId=primefaces \
  -Dversion=7.0.8 \
  -Dpackaging=jar \
  -Dfile=primefaces-7.0.8.jar
